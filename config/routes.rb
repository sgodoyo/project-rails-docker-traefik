Rails.application.routes.draw do
  resources :usuarios
  resources :licitacions
  resources :users
  resources :posts
  # get 'pages/home'
  get 'pages/about', to: 'pages#about'
  get 'pages/dashboard', to: 'pages#dashboard'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
  get 'usuarios/index', to: 'usuarios#index'
  get 'licitacions/index', to: 'licitacions#index'
  root "pages#home"
end

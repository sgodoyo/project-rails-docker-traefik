json.extract! licitacion, :id, :title, :descripcion, :fecha, :created_at, :updated_at
json.url licitacion_url(licitacion, format: :json)

FROM ruby:3.1.0
MAINTAINER Ernest Henry Shackleton <shackleton@riseup.net>
RUN apt-get update -yqq
RUN apt-get install -yqq --no-install-recommends nodejs
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN bundle install

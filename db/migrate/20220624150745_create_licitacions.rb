class CreateLicitacions < ActiveRecord::Migration[7.0]
  def change
    create_table :licitacions do |t|
      t.string :title
      t.text :descripcion
      t.date :fecha

      t.timestamps
    end
  end
end
